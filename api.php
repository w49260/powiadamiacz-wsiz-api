<?php /**
 * @author Mateusz Kucab (w49260)
 */

// sprawdzamy czy jest zmienna task
if (empty($_GET["task"])) {
	// nie ma więc błąd
	echo 'task is empty';
} else {
	$task = htmlspecialchars($_GET["task"]);
	// GŁÓWNE API
	switch ($task) {
		case "register" :
			// Rejestracja użytkownika

			// Sprawdzamy czy parametry zostały przekazane do API
			if (empty($_GET["regid"]) || empty($_GET["imienazwisko"]) || empty($_GET["stanowisko"])) {
				// Brakuje jakiegoś parametru więc wyświetlamy błąd
				echo 'params regid & imienazwisko & stanowisko is empty';
			} else {
				// Parametry są - więc zapisujemy je do zmiennych
				$regid = htmlspecialchars($_GET["regid"]);
				$imienazwisko = htmlspecialchars($_GET["imienazwisko"]);
				$stanowisko = htmlspecialchars($_GET["stanowisko"]);

				// Wywołanie funkcji do zapisywania danych o użytkowniku do bazy MySQL
				Register($imienazwisko, $stanowisko, $regid);
			}

			break;
		case "addmsg" :
			// Funkcja do dodawania wiadomości do bazy

			// Sprawdzamy czy parametry zostały przekazane do API
			if (empty($_GET["tresc"]) || empty($_GET["autor"]) || !isset($_GET["czy_niezarejstrowani_moga_widziec"])) {
				// Brakuje jakiegoś parametru więc wyświetlamy błąd
				echo 'params tresc & autor & czy_niezarejstrowani_moga_widziec is empty';
			} else {
				// Parametry są - więc zapisujemy je do zmiennych
				$tresc = htmlspecialchars($_GET["tresc"]);
				$autor = htmlspecialchars($_GET["autor"]);
				$czy_niezarejstrowani_moga_widziec = htmlspecialchars($_GET["czy_niezarejstrowani_moga_widziec"]);

				// Wywołanie funkcji do dodawania wiadomości do bazy
				AddMessages($tresc, $autor, $czy_niezarejstrowani_moga_widziec);
			}

			break;
		case "delmsg" :
			// Funkcja do usuwania komunikatu (ukrycia)
			if (empty($_GET["data"])) {
				// Brakuje jparametru więc wyświetlamy błąd
				echo 'param data is empty';
			} else {
				$data = htmlspecialchars($_GET["data"]);

				//Wywołanie funkcji do ukrywania komunikatu
				HideMessage($data);
			}

			break;
		case "getregister" :
			// Pobranie wiadomości dla użytkownika zarejestrowanego

			GetMessages(TRUE);

			break;
		case "getunregister" :
			// Pobranie wiadomości dla użytkownika niezarejestrowanego

			GetMessages(FALSE);

			break;
		default :
			echo 'bad task';
	}
}

/**
 * Funkcja do rejestracji użytkownika - zapisanie jego danych w bazie MySQL
 */
function Register($imienazwisko, $stanowisko, $regid) {
	// Podłączamy plik z bazą danych
	require "config_mysql.php";
	// Wywołujemy funkcję do łączenia się z nią
	connection();

	$zapytanie = @mysql_query("INSERT INTO users SET imie_nazwisko='$imienazwisko', stanowisko='$stanowisko', reg_id='$regid', data_rejestracji=NOW()");
	if ($zapytanie)
		echo 'ok';
	else
		echo 'error';
}

/**
 * Funkcja do pobierania wiadomości, które mają zostać wyświetlone w aplikacji
 */
function GetMessages($czy_uzytkownik_zarejestrowany) {
	// Podłączamy plik z bazą danych
	require "config_mysql.php";
	// Wywołujemy funkcję do łączenia się z nią
	connection();

	if ($czy_uzytkownik_zarejestrowany) {
		$zapytanie = mysql_query("SELECT `data` , `tresc` , `autor` FROM messages WHERE czy_widoczna=TRUE ORDER BY `data` DESC");
	} else {
		$zapytanie = mysql_query("SELECT `data` , `tresc` , `autor` FROM messages WHERE czy_widoczna=TRUE and czy_niezarejstrowani_moga_widziec=TRUE  ORDER BY `data` DESC");
	}
	$rows = array();
	while ($row = mysql_fetch_assoc($zapytanie)) {
		$rows[] = $row;
	}

	/*echo '<pre>';
	 print_r($rows);
	 echo '</pre>';*/

	// Wyświetlenie danych jsonem
	echo '{"Komunikaty": ' . json_encode($rows) . '}';
}

/**
 * Funkcja do dodawania wiadomości do bazy
 */
function AddMessages($tresc, $autor, $czy_niezarejstrowani_moga_widziec) {
	// Podłączamy plik z bazą danych
	require "config_mysql.php";
	// Wywołujemy funkcję do łączenia się z nią
	connection();

	if ($czy_niezarejstrowani_moga_widziec == "TRUE") {
		$zapytanie = @mysql_query("INSERT INTO messages SET tresc='$tresc', autor='$autor', data=NOW(), czy_niezarejstrowani_moga_widziec=TRUE");
	} else {
		$zapytanie = @mysql_query("INSERT INTO messages SET tresc='$tresc', autor='$autor', data=NOW(), czy_niezarejstrowani_moga_widziec=FALSE");
	}

	if ($zapytanie) {
		// Komunikat dodano do bazy - teraz wysłanie do serwera GCM

		// Pobieramy wszystkich usersow, którzy są aktywni
		$users = mysql_query("SELECT * FROM users WHERE czy_aktywny=TRUE");

		// Sprawdzamy czy baza w ogóle coś zwróciła
		if (mysql_num_rows($users) > 0) {
			// Wysyłamy każdemu użytkownikowi
			while ($user = mysql_fetch_assoc($users)) {
				// Wysyłamy notyfikacje
				$gcmRegIds = array($user['reg_id']);
				$message = array("message" => utf8_encode($tresc));

				sendPushNotificationToGCM($gcmRegIds, $message);
			}
		}
		echo 'ok';
		// bez względu czy wysłało do GCM czy nie -> wyświetlamy OK, bo dodało nam do bazy komunikat
	} else
		echo 'error';
}

/**
 * Funkcja do ukrywania komunikatu
 */
function HideMessage($data) {
	// Podłączamy plik z bazą danych
	require "config_mysql.php";
	// Wywołujemy funkcję do łączenia się z nią
	connection();

	$zapytanie = @mysql_query("UPDATE messages SET czy_widoczna=FALSE WHERE data='$data'");

	if ($zapytanie)
		echo 'ok';
	else
		echo 'error';
}

/**
 * Funkcja do wysyłania notyfikacji do GCM
 */
function sendPushNotificationToGCM($registatoin_ids, $message) {
	$url = 'https://android.googleapis.com/gcm/send';
	$fields = array('registration_ids' => $registatoin_ids, 'data' => $message, );
	define("GOOGLE_API_KEY", "AIzaSyDqwKvSc-d4OqjAAvphkfeTGYbv8uST17U");
	// KLUCZ API
	$headers = array('Authorization: key=' . GOOGLE_API_KEY, 'Content-Type: application/json');
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}
?>